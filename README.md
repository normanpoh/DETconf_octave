The source codes have been modified so that it can run entirely from Octave rather Matlab.

If you use the tools provided, please cite the following publication:

[Norman Poh and Samy Bengio, Database, Protocol and Tools for Evaluating Score-Level Fusion Algorithms in Biometric Authentication, Pattern Recognition, Volume 39, Issue 2, Pages 223-233, 2006. ](http://personal.ee.surrey.ac.uk/Personal/Norman.Poh/web/fusion/images/norman_benchmark.pdf)

![Output](http://personal.ee.surrey.ac.uk/Personal/Norman.Poh/web/fusion/images/analysis_2_7.gif)