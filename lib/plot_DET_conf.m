function eer = plot_DET_conf(rexpe, signs, plot_diagonal)

if nargin<2 || isempty(signs)
  signs = {'r--','r-','r--'};
end;
if nargin<3
  plot_diagonal=0;
end;

rad_list =[];
for r_=1:numel(rexpe)
  [eer(r_), tmp_, tmp_, FAR, FRR] = wer(...
    rexpe{r_}.score{1}(rexpe{r_}.label{1}==0),rexpe{r_}.score{1}(rexpe{r_}.label{1}==1));
  [deg_list, rad] = DET2polar(FAR,FRR,[],[]);
  rad_list=[rad_list; rad];
end;

set(gca,'fontsize',12);
hold on;

if plot_diagonal
  plot(ppndf([0 .8]), ppndf([0 .8]),'k:');
end;

clear tmp;
conf_interval = [25, 50, 75]/100;
for i=1:size(rad_list,2)
  tmp(:,i) = quantile(rad_list(:,i), conf_interval);
end;


lwidth = [1 2 1];
for c=1:3
  [nFAR,nFRR] = polar2DET(tmp(c,:));
  plot(nFAR, nFRR,signs{c},'linewidth',lwidth(c));
end;

if 1==0
  for i=1:size(rad_list,1)
    [nFAR,nFRR] = polar2DET(rad_list(i,:));
    plot(nFAR, nFRR,':');
  end;
end;

%legend('data', 'confidence', 'median', 'confidence' );
legend('conf', 'median', 'conf' );
Make_DET();
